<?php
function estados($estado, $success, $danger, $warning = "") {
	if ($estado == 1) {
		return '<span class="label label-success">' . $success . '</span>';
	} elseif ($estado == 0) {
		return '<span class="label label-danger">' . $danger . '</span>';
	} elseif ($estado == 2) {
        return '<span class="label label-warning">' . $warning . '</span>';
    }
}

function existencias($cantidad) {
	if ($cantidad <= 10) {
		return '<span class="label label-danger">' . $cantidad . '</span>';
	} else {
		return $cantidad;
	}
}

function fecha($fecha) {
    $corta = date("d/m/Y", $fecha);
    $larga = "Hoy a las " . date("h:i a", $fecha);
    $hoy = date("d/m/Y");
    if ($corta == $hoy) {
        return $larga;
    } else {
        return $corta;
    }
}

function create_url($name) {
	$name = trim($name);
 
    $name = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $name
    );
 
    $name = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $name
    );
 
    $name = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $name
    );
 
    $name = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $name
    );
 
    $name = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $name
    );

	$url = strtolower($name);
	$url = str_replace(" ", "-", $url);

    return $url;
}
?>