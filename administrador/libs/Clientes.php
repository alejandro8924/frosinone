<?php
require_once 'Connection.php';
/**
* 
*/
class Clientes extends Connection {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	public function login($_correo, $_clave) {

		$json = array();

		$_correo = htmlspecialchars($_correo, ENT_QUOTES);
		$_clave = md5($_clave);

		$query = "SELECT `id`, `nombre`, `apellidos`, `correo`, `clave` FROM `clientes` WHERE `correo` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('s', $_correo);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $apellidos, $correo, $clave);
			$stmt->fetch();

			if ($_correo == $correo && $_clave == $clave) {
				session_start();
				$_SESSION['id'] = $id;
				$_SESSION['nombre'] = $nombre;
				$_SESSION['correo'] = $correo;

				$json = array(
					"status" => 1,
					"description" => "login correcto"
					);
			} else {
				$json = array(
					"status" => 0,
					"description" => "error en los datos"
					);
			}
		} else {
			$json = array(
				"status" => 2,
				"description" => "error en la consulta"
				);
		}

		$stmt->close();
		return json_encode($json);
	}

	public function crearCliente($nombre, $apellidos, $correo, $clave = NULL) {

		$return = FALSE;

		$nombre = htmlspecialchars($nombre, ENT_QUOTES);

		if (!is_null($clave)) {
			$clave = md5($clave);
			$token = md5($correo) . "." . md5($clave) . "." . md5(time());
		} else {
			$clave = md5(md5(time()));
			$token = md5($correo) . "." . md5($clave) . "." . md5(time());
		}

		$query = "INSERT INTO `clientes`(`id`, `nombre`, `apellidos`, `correo`, `clave`, `token`) VALUES (NULL, ?, ?, ?, ?, ?)";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('sssss', $nombre, $apellidos, $correo, $clave, $token);
			$stmt->execute();
			$return = TRUE;
		}

		$stmt->close();
		
		return $return;
	}

	public function addInfoCliente($id, $cedula, $telefono, $direccion) {

		$return = FALSE;

		$cedula = htmlspecialchars($cedula, ENT_QUOTES);
		$telefono = htmlspecialchars($telefono, ENT_QUOTES);
		$direccion = htmlspecialchars($direccion, ENT_QUOTES);

		$query = "UPDATE `clientes` SET `cedula`= ?, `telefono`= ?, `direccion`= ? WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('sssi', $cedula, $telefono, $direccion, $id);
			$stmt->execute();
			$return = TRUE;
		}

		$stmt->close();
		
		return $return;
	}
}
?>