<?php
require_once 'Connection.php';
/**
* 
*/
class Pagos extends Connection {
	
	/*
	function __construct(argument) {
		# code...
	}
	*/

	public function generar($monto, $referencia, $pedido, $cliente) {

		$return = FALSE;

		$fecha = time();
		
		$query = "INSERT INTO `pagos`(`fecha`, `monto`, `referencia`, `pedido`, `cliente`) VALUES (?, ?, ?, ?, ?)";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('sdsii', $fecha, $monto, $referencia, $pedido, $cliente);
			$stmt->execute();
			$return = TRUE;
		}

		$stmt->close();
		return $return;
	}

	public function pagosPedido($pedido) {

		$json = array();

		$query = "SELECT `id`, `fecha`, `monto`, `referencia`, `estado` FROM `pagos` WHERE `pedido` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $pedido);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $fecha, $monto, $referencia, $estado);
			while ($stmt->fetch()) {
				array_push($json, array(
					"id" => $id,
					"fecha" => $fecha,
					"monto" => $monto,
					"referencia" => $referencia,
					"estado" => $estado
					));
			}
		}
		$stmt->close();
		return json_encode($json);
	}

	public function validarPago($id) {

		$return = FALSE;

		$query = "UPDATE `pagos` SET `estado`= 1 WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$return = TRUE;
		}

		$stmt->close();
		return $return;
	}
}
?>