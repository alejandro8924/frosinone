<?php
require_once 'Connection.php';
/**
* 
*/
class Pedidos extends Connection {
	
	/*
	function __construct() {
		# code...
	}
	*/

	public function crearPedido($cliente, $carrito = array()) {

		$fecha = time();
		$pedido = NULL;

		$query = "INSERT INTO `pedidos`(`id`, `fecha`, `cliente`) VALUES (NULL, ?, ?)";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('si', $fecha, $cliente);
			$stmt->execute();

			$pedido = $this->_mysqli->insert_id;
		}

		foreach ($carrito as $producto) {
			$query = "INSERT INTO `lineas_pedido`(`id`, `unidades`, `pedido`, `producto`) VALUES (NULL, ?, ?, ?)";
			if ($stmt = $this->_mysqli->prepare($query)) {
				$stmt->bind_param('iii', $producto["unidades"], $pedido, $producto["id"]);
				$stmt->execute();
			}
		}

		$stmt->close();
	}


	public function listaPedidos() {

		$json = array();

		$query = "SELECT `pedidos`.*, `clientes`.`nombre`, `clientes`.`apellidos` FROM `pedidos` INNER JOIN `clientes` ON `clientes`.`id` = `pedidos`.`cliente`";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $fecha, $estado, $cliente, $nombre, $apellidos);
			while ($stmt->fetch()) {
				array_push($json, array(
					"id" => $id,
					"fecha" => $fecha,
					"estado" => $estado,
					"cliente" => $cliente,
					"nombre" => $nombre,
					"apellidos" => $apellidos,
					));
			}
		}

		$stmt->close();
		return json_encode($json);
	}

	public function detallePedido($pedido) {

		$json = array();

		$productos = array();

		$query = "SELECT `pedidos`.*, `clientes`.`cedula`, `clientes`.`nombre`, `clientes`.`apellidos`, `clientes`.`correo`, `clientes`.`telefono`, `clientes`.`direccion` FROM `pedidos` LEFT JOIN `clientes` ON `pedidos`.`cliente` = `clientes`.`id` WHERE `pedidos`.`id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param("i", $pedido);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $fecha, $estado, $cliente, $cedula, $nombre, $apellidos, $correo, $telefono, $direccion);
			$stmt->fetch();

			$idPedido = $id;
			$nombreCliente = $nombre;

			$query2 = "SELECT `lineas_pedido`.*, `productos`.`nombre`, `productos`.`precio` FROM `lineas_pedido` LEFT JOIN `productos` ON `lineas_pedido`.`producto` = `productos`.`id` WHERE `lineas_pedido`.`pedido` = ?";
			if ($stmt2 = $this->_mysqli->prepare($query2)) {
				$stmt2->bind_param("i", $idPedido);
				$stmt2->execute();
				$stmt2->store_result();
				$stmt2->bind_result($id, $unidades, $pedido, $producto, $nombre, $precio);
				while ($stmt2->fetch()) {

					array_push($productos, array(
						"id" => $id,
						"unidades" => $unidades,
						"producto" => $producto,
						"nombre" => $nombre,
						"precio" => $precio
						));
				}
			}

			$json = array(
				"id" => $idPedido,
				"estado" => $estado,
				"cliente" => $cliente,
				"cedula" => $cedula,
				"nombre" => $nombreCliente,
				"apellidos" => $apellidos,
				"correo" => $correo,
				"telefono" => $telefono,
				"direccion" => $direccion,
				"productos" => $productos
				);
		}

		$stmt->close();
		return json_encode($json);
	}

	public function actualizarEstado($estado, $id) {

		$return = FALSE;

		$query = "UPDATE `pedidos` SET `estado`= ? WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param('ii', $estado, $id);
			$stmt->execute();
			$return = TRUE;
		}

		$stmt->close();
		return $return;
	}
}
?>