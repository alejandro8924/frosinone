<?php
require_once 'Connection.php';
/**
* 
*/
class Productos extends Connection {
	
	/*
	function __construct() {
		# code...
	}
	*/

	public static function create_url($name) {
		$name = trim($name);
	 
	    $name = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $name
	    );
	 
	    $name = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $name
	    );

		$url = strtolower($name);
		$url = str_replace(" ", "-", $url);

	    return $url;
	}

	public function addProducto($nombre, $descripcion, $categoria, $precio, $existencias, $imagenes = array()) {

		$return = FALSE;

		$url = self::create_url($nombre);

		$imagenes = implode(",", $imagenes);

		$query = "INSERT INTO `productos`(`nombre`, `url`, `descripcion`, `categoria`, `imagenes`, `precio`, `existencias`) VALUES (?, ?, ?, ?, ?, ?, ?)";

		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param("sssisdi", $nombre, $url, $descripcion, $categoria, $imagenes, $precio, $existencias);
			$stmt->execute();
			$return = TRUE;
		}

		$stmt->close();

		return $return;
	}

	public function modificar($id, $nombre, $descripcion, $categoria, $precio, $existencias, $imagenes = array()) {

		$return = FALSE;

		$url = self::create_url($nombre);

		$imagenes = implode(",", $imagenes);

		$query = "UPDATE `productos` SET `nombre` = ?, `url` = ?, `descripcion` = ?, `categoria` = ?, `imagenes` = ?, `precio` = ?, `existencias` = ? WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param("sssisdii", $nombre, $url, $descripcion, $categoria, $imagenes, $precio, $existencias, $id);
			$stmt->execute();
			$return = TRUE;
		}

		$stmt->close();
		return $return;
	}

	public function listaProductos() {
		
		$json = array();

		$query = "SELECT * FROM `productos`";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $url, $descripcion, $categoria, $imagenes, $colores, $precio, $existencias, $estado);
			while ($stmt->fetch()) {

				$imagenes = explode(",", $imagenes);

				array_push($json, array(
					"id" => $id,
					"nombre" => $nombre,
					"url" => $url,
					"descripcion" => $descripcion,
					"categoria" => $categoria,
					"imagenes" => $imagenes,
					"colores" => $colores,
					"precio" => $precio,
					"existencias" => $existencias,
					"estado" => $estado
					));
			}
		}

		$stmt->close();
		return json_encode($json);
	}

	public function detallesProducto($_id) {

		$json = array();

		$query = "SELECT * FROM `productos` WHERE `id` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param("i", $_id);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $url, $descripcion, $categoria, $imagenes, $colores, $precio, $existencias, $estado);
			$stmt->fetch();

			$imagenes = explode(",", $imagenes);

				$json = array(
					"id" => $id,
					"nombre" => $nombre,
					"url" => $url,
					"descripcion" => $descripcion,
					"categoria" => $categoria,
					"imagenes" => $imagenes,
					"colores" => $colores,
					"precio" => $precio,
					"existencias" => $existencias,
					"estado" => $estado
					);
		}

		$stmt->close();
		return json_encode($json);
	}

	public function deUltima() {
		$json = array();

		$query = "SELECT `id`, `nombre`, `url`, `categoria`, `imagenes`, `precio` FROM `productos` ORDER BY `id` DESC LIMIT 4";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $url, $categoria, $imagenes, $precio);
			while ($stmt->fetch()) {

				$imagenes = explode(",", $imagenes);

				array_push($json, array(
					"id" => $id,
					"nombre" => $nombre,
					"url" => $url,
					"categoria" => $categoria,
					"imagenes" => $imagenes,
					"precio" => $precio
					));
			}
		}

		$stmt->close();
		return json_encode($json);
	}

	public function relacionados($_id, $_nombre) {
		$json = array();

		$query = "SELECT `id`, `nombre`, `url`, `categoria`, `imagenes`, `precio` FROM `productos` WHERE `nombre` LIKE '%" . $_nombre . "%' AND `id` <> ? LIMIT 4";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param("i", $_id);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $url, $categoria, $imagenes, $precio);
			while ($stmt->fetch()) {

				$imagenes = explode(",", $imagenes);

				array_push($json, array(
					"id" => $id,
					"nombre" => $nombre,
					"url" => $url,
					"categoria" => $categoria,
					"imagenes" => $imagenes,
					"precio" => $precio
					));
			}
		}

		$stmt->close();
		return json_encode($json);
	}

	public function categoria($categoria) {

		$json = array();

		$query = "SELECT `id`, `nombre`, `url`, `imagenes`, `precio` FROM `productos` WHERE `categoria` = ?";
		if ($stmt = $this->_mysqli->prepare($query)) {
			$stmt->bind_param("i", $categoria);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($id, $nombre, $url, $imagenes, $precio);
			while ($stmt->fetch()) {

				$imagenes = explode(",", $imagenes);

				array_push($json, array(
					"id" => $id,
					"nombre" => $nombre,
					"url" => $url,
					"imagenes" => $imagenes,
					"precio" => $precio
					));
			}
		}

		$stmt->close();
		return json_encode($json);
	}
}
?>