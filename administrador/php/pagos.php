<?php
if (!empty($_POST['id']) && !empty($_POST['estado'])) {
	require_once '../libs/Pagos.php';
	$pagos = new Pagos();

	if ($pagos->validarPago($_POST['id'])) {
		echo json_encode(array(
			"status" => 1,
			"description" => "El pago de valido satisfactoriamente"
			));
	} else {
		echo json_encode(array(
			"status" => 0,
			"description" => "No se pudo validar el pago"
			));
	}
}
?>