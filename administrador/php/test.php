<?php
require_once "../libs/Productos.php";
require_once "../libs/functions.php";
$productos = new Productos();

if (!empty($_POST['nombre']) && !empty($_POST['descripcion']) && !empty($_POST['categoria']) && !empty($_POST['precio']) && !empty($_POST['existencias']) && isset($_FILES['file1'])) {

	$nombre = create_url($_POST['nombre']);
	$imagenes = array();
	
	array_push($imagenes, $nombre . "-0.jpg");

	if (!empty($_FILES['file2']) && isset($_FILES['file2'])) {
		array_push($imagenes, $nombre . "-1.jpg");
	}

	if (!empty($_FILES['file3']) && isset($_FILES['file3'])) {
		array_push($imagenes, $nombre . "-2.jpg");
	}

	if (!empty($_FILES['file4']) && isset($_FILES['file4'])) {
		array_push($imagenes, $nombre . "-3.jpg");
	}

	if ($productos->addProducto($_POST['nombre'], $_POST['descripcion'], $_POST['categoria'], $_POST['precio'], $_POST['existencias'], $imagenes)) {
		
		$carpeta = "../images/productos/" . $nombre;

		if (!file_exists($carpeta)) {
			mkdir($carpeta);
		}

		move_uploaded_file($_FILES['file1']['tmp_name'], $carpeta . "/" . $nombre . "-0.jpg");

		if (!empty($_FILES['file2']) && isset($_FILES['file2'])) {
			move_uploaded_file($_FILES['file2']['tmp_name'], $carpeta . "/" . $nombre . "-1.jpg");
		}

		if (!empty($_FILES['file3']) && isset($_FILES['file3'])) {
			move_uploaded_file($_FILES['file3']['tmp_name'], $carpeta . "/" . $nombre . "-2.jpg");
		}

		if (!empty($_FILES['file4']) && isset($_FILES['file4'])) {
			move_uploaded_file($_FILES['file4']['tmp_name'], $carpeta . "/" . $nombre . "-3.jpg");
		}

		echo json_encode(array(
			"status" => 1,
			"description" => "El producto se guardó satisfactoriamente"
			));
	} else {
		echo json_encode(array(
			"status" => 0,
			"description" => "Ha ocurrido un error inesperado, inténtalo de nuevo, verifica que los campos no estén vacíos"
			));
	}

} else {
	echo json_encode(array(
		"status" => 0,
		"description" => "Hay campos vacíos que son necesarios"
		));
}
?>