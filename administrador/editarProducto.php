<?php
require_once 'libs/Productos.php';

$productos = new Productos();

$detalles = json_decode($productos->detallesProducto($_GET['id']));
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/styles.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar">
				<?php include 'inc/main-sidebar.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Productos <small>Modificar Producto</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-book"></i> Catálogo</a></li>
						<li class="active">Productos</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-md-12">
							<div id="alerta">
							</div>
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Datos del producto</h3>
								</div>
								<form id="formulario" action="php/test.php" method="POST" enctype="multipart/form-data" autocomplete="off">
									<div class="box-body">
										<div class="row">
											<div class="col-md-5">
												<div class="form-group">
													<label for="nombre">Nombre:</label>
													<input type="text" id="nombre" name="nombre" value="<?php echo $detalles->nombre; ?>" class="form-control">
												</div>
												<div class="form-group">
													<label for="nombre">Descripcion:</label>
													<textarea name="descripcion" id="descripcion" class="form-control"><?php echo $detalles->nombre; ?></textarea>
												</div>
												<div class="form-group">
													<label for="nombre">Categorias:</label>
													<select name="categoria" id="categoria" class="form-control">
														<option value="<?php echo $detalles->categoria; ?>"><?php echo $detalles->categoria; ?></option>
														<option value="1">Hombres</option>
														<option value="2">Mujeres</option>
														<option value="3">Accesorios</option>
														<option value="4">Sport</option>
														<option value="5">Nylon</option>
													</select>
												</div>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label for="precio">Precio:</label>
															<input type="text" id="precio" name="precio" value="<?php echo $detalles->precio; ?>" class="form-control">
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label for="existencias">Existencias:</label>
															<input type="text" id="existencias" name="existencias" value="<?php echo $detalles->existencias; ?>" class="form-control">
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-7">
												<div class="row">
													<div class="div col-sm-3">
														<div class="fallback">
															<input type="file" name="file1" id="file1" />
															<div class="preview" style="background-image: url('images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[0]; ?>');">
																		
															</div>
														</div>
													</div>
													<div class="div col-sm-3">
														<div class="fallback">
															<input type="file" name="file2" id="file2" />
															<div class="preview" style="background-image: url('images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[1]; ?>');">
																		
															</div>
														</div>
													</div>
													<div class="div col-sm-3">
														<div class="fallback">
															<input type="file" name="file3" id="file3" />
															<div class="preview" style="background-image: url('images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[2]; ?>');">
																		
															</div>
														</div>
													</div>
													<div class="div col-sm-3">
														<div class="fallback">
															<input type="file" name="file4" id="file4" />
															<div class="preview" style="background-image: url('images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[3]; ?>');">
																		
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="box-footer">
										<input type="hidden" name="id" value="<?php echo $detalles->id; ?>">
										<button type="button" id="editProducto" class="btn btn-primary">Modificar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src=="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.min.js"></script>
		<script type="text/javascript" src="dist/js/demo.js"></script>
		<script type="text/javascript" src="dist/js/pages/addProducto.js"></script>
	</body>
</html>