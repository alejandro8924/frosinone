<?php
require_once 'libs/Pedidos.php';
require_once 'libs/Pagos.php';
require_once 'libs/functions.php';
$pedidos = new Pedidos();
$pagos = new Pagos();
$lista = json_decode($pedidos->detallePedido($_GET['id']));
$listaPagos = json_decode($pagos->pagosPedido($_GET['id']));
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar">
				<?php include 'inc/main-sidebar.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Dashboard <small>Control panel</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</section>
				<div class="content">
					<div class="invoice">
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header"> <i class="fa fa-globe"></i> Frosinone C.A. <small class="pull-right">Fecha: <?php echo date("d/m/Y"); ?></small></h2>
							</div>
						</div>
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								De
								<address>
									<strong>Frosinone C.A.</strong><br>
									795 Folsom Ave, Suite 600<br>
									San Francisco, CA 94107<br>
									Telefono: (804) 123-5432<br>
									Email: info@frosinonefabrica.com
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								Para
								<address>
									<strong><?php echo $lista->nombre . " " . $lista->apellidos; ?></strong><br>
									<?php echo $lista->direccion; ?><br>
									Email: <?php echo $lista->correo; ?>
								</address>
							</div>
							<div class="col-sm-4 invoice-col">
								<b>Orden ID:</b> <?php echo $lista->id; ?><br>
								<b>Fecha de pago:</b> 2/22/2014<br>
								<b>Cuenta:</b> 968-34567
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>ID</th>
											<th>Producto</th>
											<th>Precio</th>
											<th>unidades</th>
											<th>Subtotal</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$total = 0;
										for ($i=0; $i < count($lista->productos); $i++) {
										?>
										<tr>
											<td><?php echo $lista->productos[$i]->producto; ?></td>
											<td><?php echo $lista->productos[$i]->nombre; ?></td>
											<td><?php echo number_format($lista->productos[$i]->precio, 2, ',', '.'); ?></td>
											<td><?php echo $lista->productos[$i]->unidades; ?></td>
											<td><?php echo number_format($lista->productos[$i]->precio * $lista->productos[$i]->unidades, 2, ',', '.'); ?></td>
										</tr>
										<?php
										$total = $total + ($lista->productos[$i]->precio * $lista->productos[$i]->unidades);
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6">
								<p class="lead">Pago realizados:</p>
								<table class="table table-bordered">
									<tr>
										<th style="width: 10px">#</th>
										<th>Fecha</th>
										<th>Monto</th>
										<th>Referencia</th>
										<th>Estado</th>
										<th>Validar</th>
									</tr>
									<?php for ($i=0; $i < count($listaPagos); $i++) { ?>
									<tr>
										<td><?php echo $listaPagos[$i]->id; ?></td>
										<td><?php echo fecha($listaPagos[$i]->fecha); ?></td>
										<td><?php echo number_format($listaPagos[$i]->monto, 2, ',', '.'); ?></td>
										<td><?php echo $listaPagos[$i]->referencia; ?></td>
										<th id="estadoDePago"><?php echo estados($listaPagos[$i]->estado, "validado", "pendiente"); ?></th>
										<th><?php if (!$listaPagos[$i]->estado) { ?><a href="#" class="validarPago" data-id="<?php echo $listaPagos[$i]->id; ?>" data-estado="1">Validar</a><?php } ?></th>
									</tr>
									<?php } ?>
								</table>
								<div id="alertaValidarPago">
								</div>
							</div>
							<div class="col-xs-6">
								<p class="lead">monto a pagar:</p>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th>Total:</th>
											<td><?php echo number_format($total, 2, ',', '.'); ?> Bs.</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="row no-print">
							<div class="col-xs-12">
								<button class="btn btn-success pull-right"><i class="fa fa-truck"></i> Despachar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src=="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.min.js"></script>
		<script type="text/javascript" src="dist/js/demo.js"></script>
		<script type="text/javascript" src="dist/js/pages/pagos.js"></script>
	</body>
</html>