$('input[type="file"]').on('change', function() {

    var id = $(this).attr('id');

    var archivos = document.getElementById(id).files;
    var navegador = window.URL || window.webkitURL;

    for (var i = 0; i < archivos.length; i++) {
        var nombre = archivos[i].name;

        var objeto_url = navegador.createObjectURL(archivos[i]);
        //$('.fallback').append('<div class="preview" style="background-image: url(' + objeto_url + ');"></div>');
        //$(this).after('<div class="preview" style="background-image: url(' + objeto_url + ');"></div>');
        $(this).siblings('.preview').css('background-image', 'url(' + objeto_url + ')');
    };
});

$('#addProducto').click(function() {
	var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: 'php/test.php',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function() {
            console.log('Cargando...');
            $('#alerta').html('<div class="callout callout-info"><h4>¡Procesando!</h4><p>Modificando el producto en la tienda.</p></div>');
        },
        success: function(data) {
            console.log(data);
            if (data.status == 1) {
                $('input').val('');
                $('textarea').val('');
                $('.preview').css('background-image', 'url("")');
                $('#alerta').html('<div class="callout callout-success"><h4>¡Éxito!</h4><p>' + data.description + '.</p></div>');
            } else {
                $('#alerta').html('<div class="callout callout-danger"><h4>¡Error!</h4><p>' + data.description + '.</p></div>');
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
            $('#alerta').html('<div class="callout callout-danger"><h4>¡Error!</h4><p>Ha ocurrido un error inesperado, inténtalo de nuevo, verifica que los campos no estén vacíos.</p></div>');
        }
    });
});

$('#editProducto').click(function() {
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: 'php/modificarProducto.php',
        type: 'POST',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function() {
            console.log('Cargando...');
            $('#alerta').html('<div class="callout callout-info"><h4>¡Procesando!</h4><p>Guardando el producto en la tienda.</p></div>');
        },
        success: function(data) {
            console.log(data);
            if (data.status == 1) {
                $('#alerta').html('<div class="callout callout-success"><h4>¡Éxito!</h4><p>' + data.description + '.</p></div>');
            } else {
                $('#alerta').html('<div class="callout callout-danger"><h4>¡Error!</h4><p>' + data.description + '.</p></div>');
            };
        },
        timeout: 30000,
        error: function(err) {
            console.log(err);
            $('#alerta').html('<div class="callout callout-danger"><h4>¡Error!</h4><p>Ha ocurrido un error inesperado, inténtalo de nuevo, verifica que los campos no estén vacíos.</p></div>');
        }
    });
});