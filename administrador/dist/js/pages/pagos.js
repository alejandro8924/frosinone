$('.validarPago').click(function() {
    $.ajax({
        url: 'php/pagos.php',
        type: 'POST',
        dataType: 'json',
        data: {
            id: $(this).data('id'),
            estado: $(this).data('estado')
        },
        beforeSend: function() {
        	$('#alertaValidarPago').html('<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>	<i class="icon fa fa-check"></i> Error!</h4>Validando el pago</div>');
        },
        success: function(data) {
        	if (data.status == 1) {
        		$(this).remove();
        		$('#estadoDePago').html('<span class="label label-success">validado</span>');
        		$('#alertaValidarPago').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>	<i class="icon fa fa-check"></i> Éxito!</h4>' + data.description + '</div>');
        	} else {
        		$('#alertaValidarPago').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4>	<i class="icon fa fa-check"></i> Error!</h4>' + data.description + '</div>');
        	};
        },
        timeout: 30000,
        error: function(err) {
        	console.log(err);
        }
    });
});