<?php
require_once 'libs/functions.php';
require_once 'libs/Pedidos.php';

$pedidos = new Pedidos();

$lista = json_decode($pedidos->listaPedidos());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="plugins/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.min.css">
		<link rel="stylesheet" type="text/css" href="dist/css/skins/_all-skins.min.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include 'inc/main-header.php'; ?>
			</header>
			<aside class="main-sidebar">
				<?php include 'inc/main-sidebar.php'; ?>
			</aside>
			<div class="content-wrapper">
				<section class="content-header">
					<h1>Dashboard <small>Control panel</small></h1>
					<ol class="breadcrumb">
						<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active">Dashboard</li>
					</ol>
				</section>
				<section class="content">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<h3>Pedidos</h3>
								</div>
								<div class="box-body">
									<table id="example2" class="table table-bordered table-hover">
										<thead>
											<tr>
												<th>ID</th>
												<th>Fecha</th>
												<th>Estado</th>
												<th>Cliente</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody>
											<?php for ($i=0; $i < count($lista); $i++) { ?>
											<tr>
												<td><?php echo $lista[$i]->id; ?></td>
												<td><?php echo fecha($lista[$i]->fecha); ?></td>
												<td><?php echo estados($lista[$i]->estado, "Tramitado", "Pendiente", "En proceso"); ?></td>
												<td><?php echo $lista[$i]->nombre . " " . $lista[$i]->apellidos; ?></td>
												<td><a href="pedido.php?id=<?php echo $lista[$i]->id; ?>">Ver pedido</a></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<footer class="main-footer">
				<?php include 'inc/main-footer.php'; ?>
			</footer>
		</div>
		<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script type="text/javascript" src=="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="plugins/datatables/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="plugins/fastclick/fastclick.min.js"></script>
		<script type="text/javascript" src="dist/js/app.min.js"></script>
		<script type="text/javascript" src="dist/js/demo.js"></script>
		<script type="text/javascript">
		$(function() {
			$('#example2').DataTable({
				"paging": true,
				"lengthChange": false,
				"searching": false,
				"ordering": true,
				"info": true,
				"autoWidth": false
			});
		});
		</script>
	</body>
</html>