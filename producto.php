<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Frosinone | CARTERA MANANTIAL</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/menu-dist.css">
		<link rel="stylesheet" type="text/css" href="css/styles-dist.css">
		<!--<link rel="stylesheet" type="text/css" href="css/styles.css">-->
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
	</head>
	<body>
		<div id="openCar">
			<span><span class="icon-shop"></span></span>
		</div>
		<?php include 'inc/carrito.php'; ?>
		<!--
		<header class="main-header" style="background-color: rgba(255, 255, 255, 0.70);position: fixed;top: 0;z-index: 99;">
			<?php //include 'inc/main-header.php'; ?>
		</header>
		-->
		<section id="articulo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div id="images">
							<figure>
								<img src="img/mv/DSC_5211.jpg" alt="" class="img-responsive">
							</figure>
						</div>
						<div class="row">
							<div class="col-xs-4">
								<figure class="miniaturas">
									<a href="#">
										<img src="img/mv/DSC_5211.jpg" alt="" class="img-responsive">
									</a>
								</figure>
							</div>
							<div class="col-xs-4">
								<figure class="miniaturas">
									<a href="#">
										<img src="img/mv/DSC_5211.jpg" alt="" class="img-responsive">
									</a>
								</figure>
							</div>
							<div class="col-xs-4">
								<figure class="miniaturas">
									<a href="#">
										<img src="img/mv/DSC_5211.jpg" alt="" class="img-responsive">
									</a>
								</figure>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<h1>CARTERA MANANTIAL</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
				</div>
			</div>
		</section>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<script type="text/javascript" src="js/carrito.js"></script>
	</body>
</html>