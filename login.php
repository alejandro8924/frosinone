<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Frosinone</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/menu-dist.css">
		<link rel="stylesheet" type="text/css" href="css/styles-dist.css">
		<!--<link rel="stylesheet" type="text/css" href="css/styles.css">-->
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
	</head>
	<body>
		<header class="main-header">
			<?php include 'inc/main-header.php'; ?>
		</header>
		<section id="login">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<form action="php/login.php" method="POST">
							<div class="form-group">
								<label for="correo">Correo Electronico</label>
								<input type="email" class="form-control" id="correo" name="correo" placeholder="correo@dominio.com">
							</div>
							<div class="form-group">
								<label for="clave">Contraseña</label>
								<input type="password" class="form-control" id="clave" name="clave" placeholder="Contraseña">
								<?php if (!empty($_GET['compra']) && isset($_GET['compra'])) { if ($_GET['compra'] == "1") { ?>
								<input type="hidden" name="compra" value="1">
								<?php } } ?>
							</div>
							<button type="submit" class="btn btn-default">Entrar</button>
						</form>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>