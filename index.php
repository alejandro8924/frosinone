<?php
require_once 'administrador/libs/Productos.php';
$productos = new Productos();
$lista = json_decode($productos->deUltima());
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Frosinone</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/menu-dist.css">
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
	</head>
	<body>
		<header class="main-header">
			<?php include 'inc/main-header.php'; ?>
		</header>
		<section id="home">
			<div id="carousel-example" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-example" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example" data-slide-to="1"></li>
					<li data-target="#carousel-example" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="item active">
						<a href="#"><img src="administrador/images/slider/Slider1.jpg" /></a>
						<div class="carousel-caption">
							<!--
							<h3>Meow</h3>
							<p>Just Kitten Around</p>
							-->
						</div>
					</div>
					<div class="item">
						<a href="#"><img src="administrador/images/slider/Slider2.jpg" /></a>
						<div class="carousel-caption">
							<!--
							<h3>Meow</h3>
							<p>Just Kitten Around</p>
							-->
						</div>
					</div>
					<div class="item">
						<a href="#"><img src="administrador/images/slider/Slider3.jpg" /></a>
						<div class="carousel-caption">
							<!--
							<h3>Meow</h3>
							<p>Just Kitten Around</p>
							-->
						</div>
					</div>
				</div>
				<a class="left carousel-control" href="#carousel-example" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-example" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</section>
		<section id="bestSeller">
			<div class="container">
				<div class="row">
					<div class="col-ms-12">
						<h2>De ultima</h2>
					</div>
				</div>
				<div class="row">
					<?php for ($i=0; $i < count($lista); $i++) { ?>
					<div class="col-sm-3">
						<div class="articulo">
							<a href="producto/<?php echo $lista[$i]->id; ?>/<?php echo $lista[$i]->url; ?>">
								<figure>
									<img src="administrador/images/productos/<?php echo $lista[$i]->url; ?>/<?php echo $lista[$i]->imagenes[0]; ?>" alt="<?php echo $lista[$i]->nombre; ?>" class="img-responsive">
								</figure>
								<span class="nombre"><?php echo $lista[$i]->nombre; ?></span>
								<br>
								<span class="precio"><?php echo $lista[$i]->precio; ?> Bs.</span>
							</a>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-6 no-space">
					<div class="grid">
						<div class="details">
							<h2>Amy</h2>
							<span class="referencia">Ref. 375</span>
						</div>
						<figure>
							<img src="administrador/images/inicio/DSC_5109.jpg" alt="" class="img-responsive">
						</figure>
					</div>
				</div>
				<div class="col-sm-6 no-space">
					<div class="grid">
						<div class="details">
							<h2>Billetera Triptica</h2>
							<span class="referencia">Red. mq025</span>
						</div>
						<figure>
							<img src="administrador/images/inicio/DSC_5154.jpg" alt="" class="img-responsive">
						</figure>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3 no-space">
					<div class="grid">
						<div class="details">
							<h2>Portacos Meticos</h2>
							<span class="referencia">Ref. mq290</span>
						</div>
						<figure>
							<img src="administrador/images/inicio/DSC_5435.jpg" alt="" class="img-responsive">
						</figure>
					</div>
				</div>
				<div class="col-sm-6 no-space">
					<div class="grid">
						<div class="details">
							<h2>Fitness Bag</h2>
							<span class="referencia">Ref. bj142</span>
						</div>
						<figure>
							<img src="administrador/images/inicio/DSC_5348.jpg" alt="" class="img-responsive">
						</figure>
					</div>
				</div>
				<div class="col-sm-3 no-space">
					<div class="grid">
						<div class="details">
							<h2>Cartera Manantial</h2>
							<span class="referencia">Ref. ca368</span>
						</div>
						<figure>
							<img src="administrador/images/inicio/DSC_5260.jpg" alt="" class="img-responsive">
						</figure>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="quienesSomos" class="col-sm-6">
					<p>Somos una empresa amante del trabajo artesanal en cuero. Trasmitimos a través de cada pieza deseos y emociones intensas. Cada producto es elaborado por manos venezolanas con la intención de  sorprender y enamorar a sus clientes a través de los sentidos.</p>
					<p>En Frosinone encontraras moda y elegancia que complementaran tu estilo.</p>
				</div>
				<div id="tituloQuienesSomos" class="col-sm-6 no-space">
					<div class="triangulo">
						
					</div>
					<span>Quienes Somos</span>
				</div>
			</div>
			<div class="row">
				<div id="misionVision" class="col-sm-6">
					<div class="row">
						<div id="textoMisionVision" class="col-sm-6 col-sm-offset-6">
							<div class="triangulo-2">
								
							</div>
							<h4>MISION</h4>
							<p>Satisfacer a nuestros clientes con artículos clásicos y de moda,  elaborados en el más puro cuero  y materiales de vanguardia  en  bolsos, carteras, correas, peletería, maletas y accesorios.  Buscando la diferenciación en diseño, calidad y servicio.</p>
							<h4>VISION</h4>
							<p>En FROSINONE  actualmente orientamos todos nuestros esfuerzos para alcanzar en el año 2018  el posicionamiento de nuestra marca mediante una amplia red de distribución a todo lo ancho del territorio nacional en las mejores tiendas de marroquinería y  ofrecer una nueva puerta de negocios a través de las redes sociales  y pagina web dando asistencia en ventas directas a nuestros clientes.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row">
						<div id="historia" class="col-sm-12">
							<span>Un poco de historia</span>
							<div class="triangulo">
								
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<h4>VALORES</h4>
							<p>En FROSINONE basamos nuestros esfuerzos en valores como son el compromiso al trabajo constante, la responsabilidad, la honestidad y el respeto tanto a nuestros  clientes, como a nuestros trabajadores,  proveedores y el entorno donde nos desenvolvemos.</p>
							<h4>UN POCO DE HISTORIA</h4>
							<p>La marca FROSINONE nace del nombre del pueblo en Italia de donde es nato su fundador principal, el Sr. Dino  Giovannone.  El, unido a su esposa  Beatriz González y su  socio Manuel Maza,  también fundador, crean esta empresa en el año 1995,  con la visión de crecimiento y compromiso de trabajo. Durante años ha existido el compromiso de aprender y perfeccionar el arte de la peletería y confeccionar  bellos y creativos artículos en cuero.  Muchos han sido los esfuerzos por asistir a ferias internacionales con el propósito de importar moda y tecnología de vanguardia. El resultado a través del tiempo es un producto que goza del reconocimiento por ser moda, calidad y buen servicio.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<?php include 'inc/main-footer.php'; ?>
		</footer>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<script type="text/javascript" src="js/carrito-dist.js"></script>
		<!--<script type="text/javascript" src="js/carrito.js"></script>-->
	</body>
</html>