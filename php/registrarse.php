<?php
require_once '../administrador/libs/Clientes.php';

$clientes = new Clientes();

if ($clientes->crearCliente($_POST['nombre'], $_POST['apellidos'], $_POST['correo'], $_POST['clave'])) {
	header("Location: ../infoRegistro.php?info=success");
} else {
	header("Location: ../infoRegistro.php?info=error");
}
?>