<?php
@session_start();
if (!isset($_SESSION['id']) && !isset($_SESSION['nombre']) && !isset($_SESSION['correo'])) {
	session_destroy();
	header("Location: index.php");
}
?>