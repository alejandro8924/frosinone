<?php
session_start();
if (empty($_SESSION['correo']) || !isset($_SESSION['correo'])) {

	header("Location: ../login.php?compra=1");

} else {
	
	require_once '../administrador/libs/Carrito.php';
	require_once '../administrador/libs/Pedidos.php';
	require_once '../administrador/libs/PHPMailer/class.phpmailer.php';
	
	$carrito = new Carrito();
	$pedidos = new Pedidos();
	
	$carro = $carrito->get_content();

	$productos = array();
	$total = 0;

	if ($carro) {
		$pedidos->crearPedido($_SESSION['id'], $carro);

		foreach ($carro as $producto) {
			array_push($productos, '
				<table border="0"
				    cellpadding="0"
				    cellspacing="0" width=
				    "100%">
				    <tr valign="top">
				        <td style=
				            "width:90px">
				            <span style=
				                "border:1px solid #cccccc;display:block">
				            <a data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=https://myaccount.mercadolibre.com.ve/sales/vop?orderId%3D1068562564&amp;source=gmail&amp;ust=1472664711076000&amp;usg=AFQjCNGefKh8fLMSPqzlACRTbxUg0cERDA"
				                href=
				                "https://myaccount.mercadolibre.com.ve/sales/vop?orderId=1068562564"
				                target=
				                "_blank"><img
				                src=
				                "' . $producto['imagen'] . '"
				                style=
				                "display:block"
				                width="100"></a></span>
				        </td>
				        <td>
				            <table border="0"
				                cellpadding="0"
				                cellspacing="0">
				                <tr>
				                    <td style="font-family:Arial;font-size:14px;color:#666666;padding:0 20px 6px 15px;line-height:1.4">
				                        ' . $producto['nombre'] . '
				                    </td>
				                </tr>
				                <tr>
				                    <td style="font-family:Arial;font-size:12px;color:#999999;padding:0 20px 6px 15px">
				                        Cantidad:
				                        ' . $producto['unidades'] . '
				                    </td>
				                </tr>
				                <tr>
				                    <td style="font-family:Arial;font-size:16px;color:#b22c00;padding:0 20px 0 15px">
				                        Bs.
				                        ' . $producto['precio'] . '
				                        <span style="font-family:Arial;font-size:14px;color:#666666">
				                        c/u</span>
				                    </td>
				                </tr>
				            </table>
				        </td>
				    </tr>
				</table>
				');

			$total = $total + $producto['total'];
		}
	}


	$mail = new PHPMailer(); // defaults to using php "mail()"

	$body = file_get_contents('../correoCompra.html');
	$body = str_replace("[\]", '', $body);

	$articulos = implode(" ", $productos);

	$body = str_replace("!#PRODUCTO#!", $articulos, $body);
	$body = str_replace("!#TOTAL#!", $total, $body);

	$mail->SetFrom('info@frosinonefabrica.com', 'frosinone');

	$address = $_SESSION['correo'];
	$mail->AddAddress($address, $_SESSION['nombre']);

	$mail->Subject = "Gracias por su compra";

	$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

	$mail->MsgHTML($body);

	if(!$mail->Send()) {
		//$carrito->destroy();
		echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
		$carrito->destroy();
		header("Location: ../compra.php");
	}
}
?>