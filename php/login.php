<?php
require_once '../administrador/libs/Clientes.php';

$clientes = new Clientes();

$acceso = json_decode($clientes->login($_POST['correo'], $_POST['clave']));

if ($acceso->status == 1) {
	if (!empty($_POST['compra']) && isset($_POST['compra'])) {
		if ($_POST['compra'] == "1") {
			header("Location: comprar.php");
		} else {
			header("Location: ../index.php");
		}
	} else {
		header("Location: ../index.php");
	}
}
?>