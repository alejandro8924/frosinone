<?php
session_start();
require_once '../administrador/libs/Carrito.php';
$carrito = new Carrito();


if (!empty($_POST['id']) && !empty($_POST['nombre']) && !empty($_POST['precio']) && !empty($_POST['cantidad'])) {

$articulo = array(
	"id" => $_POST['id'],
	"unidades" => $_POST['cantidad'],
	"precio" =>  $_POST['precio'],
	"nombre" =>  $_POST['nombre'],
	"imagen" =>  $_POST['imagen']
	);

$carrito->add($articulo);

//echo json_encode($carrito->get_content());

$carro = $carrito->get_content();

$unidades = 0;
?>
<span class="icon-cancel-circle close"></span>
<div class="row">
	<?php if ($carro) { ?>
	<?php
	foreach ($carro as $producto) {
	$unidades = $unidades + $producto['unidades'];
	?>
	<div class="col-xs-12">
		<div class="col-xs-4">
			<figure>
				<img src="<?php echo $producto["imagen"]; ?>" alt="" class="img-responsive">
			</figure>
		</div>
		<div class="col-xs-8">
			<h4><?php echo $producto["nombre"]; ?></h4>
			<span id="removeItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> - </span> <?php echo $producto["unidades"]; ?> <span id="addItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> + </span> x <?php echo number_format($producto["precio"], 2, ',', '.'); ?> Bs.
			<span class="eliminarProducto" data-unico="<?php echo $producto['unique_id']; ?>">Eliminar</span>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
</div>
<div class="totalCarrito">
	<div class="row">
		<div class="col-xs-12">
			<h4>Subtotal: <span><?php echo number_format($carrito->precio_total(), 2, ',', '.'); ?> Bs.</span></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php if (count($carro) > 5 || $unidades > 5) { ?>
			<a href="../../php/comprar.php" class="btn btn-carrito" data-estado="true">Procesar pedido</a>
			<?php } else { ?>
			<span class="btn btn-carrito" data-estado="false">Debes tener al menos 6 productos <br> para procesar el pedido</span>
			<?php } ?>
			<button id="vaciarCarrito" class="btn btn-carrito vaciar">Vaciar carrito</button>
		</div>
	</div>
</div>
<?php
} elseif (!empty($_POST['vaciar'])) {
$carrito->destroy();
$carro = $carrito->get_content();
$unidades = 0;
?>
<span class="icon-cancel-circle close"></span>
<div class="row">
	<?php if ($carro) { ?>
	<?php
	foreach ($carro as $producto) {
	$unidades = $unidades + $producto['unidades'];
	?>
	<div class="col-xs-12">
		<div class="col-xs-4">
			<figure>
				<img src="<?php echo $producto["imagen"]; ?>" alt="" class="img-responsive">
			</figure>
		</div>
		<div class="col-xs-8">
			<h4><?php echo $producto["nombre"]; ?></h4>
			<span id="removeItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> - </span> <?php echo $producto["unidades"]; ?> <span id="addItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> + </span> x <?php echo number_format($producto["precio"], 2, ',', '.'); ?> Bs.
			<span class="eliminarProducto" data-unico="<?php echo $producto['unique_id']; ?>">Eliminar</span>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
</div>
<div class="totalCarrito">
	<div class="row">
		<div class="col-xs-12">
			<h4>Subtotal: <span><?php echo number_format($carrito->precio_total(), 2, ',', '.'); ?> Bs.</span></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php if (count($carro) > 5 || $unidades > 5) { ?>
			<a href="../../php/comprar.php" class="btn btn-carrito" data-estado="true">Procesar pedido</a>
			<?php } else { ?>
			<span class="btn btn-carrito" data-estado="false">Debes tener al menos 6 productos <br> para procesar el pedido</span>
			<?php } ?>
			<button id="vaciarCarrito" class="btn btn-carrito vaciar">Vaciar carrito</button>
		</div>
	</div>
</div>
<?php
} elseif (!empty($_POST['uniqueId']) && !empty($_POST['eliminarProducto'])) {
$carrito->remove_producto($_POST['uniqueId']);
$carro = $carrito->get_content();
$unidades = 0;
?>
<span class="icon-cancel-circle close"></span>
<div class="row">
	<?php if ($carro) { ?>
	<?php
	foreach ($carro as $producto) {
	$unidades = $unidades + $producto['unidades'];
	?>
	<div class="col-xs-12">
		<div class="col-xs-4">
			<figure>
				<img src="<?php echo $producto["imagen"]; ?>" alt="" class="img-responsive">
			</figure>
		</div>
		<div class="col-xs-8">
			<h4><?php echo $producto["nombre"]; ?></h4>
			<span id="removeItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> - </span> <?php echo $producto["unidades"]; ?> <span id="addItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> + </span> x <?php echo number_format($producto["precio"], 2, ',', '.'); ?> Bs.
			<span class="eliminarProducto" data-unico="<?php echo $producto['unique_id']; ?>">Eliminar</span>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
</div>
<div class="totalCarrito">
	<div class="row">
		<div class="col-xs-12">
			<h4>Subtotal: <span><?php echo number_format($carrito->precio_total(), 2, ',', '.'); ?> Bs.</span></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php if (count($carro) > 5 || $unidades > 5) { ?>
			<a href="../../php/comprar.php" class="btn btn-carrito" data-estado="true">Procesar pedido</a>
			<?php } else { ?>
			<span class="btn btn-carrito" data-estado="false">Debes tener al menos 6 productos <br> para procesar el pedido</span>
			<?php } ?>
			<button id="vaciarCarrito" class="btn btn-carrito vaciar">Vaciar carrito</button>
		</div>
	</div>
</div>
<?php
} elseif (!empty($_POST['uniqueId']) && !empty($_POST['modificarItem']) && !empty($_POST['accion'])) {

if ($_POST['accion'] == "addItem") {
	$carrito->add_cantidad_producto($_POST['uniqueId']);
} elseif($_POST['accion'] == "removeItem") {
	$carrito->remove_cantidad_producto($_POST['uniqueId']);
}
$carro = $carrito->get_content();
$unidades = 0;
?>
<span class="icon-cancel-circle close"></span>
<div class="row">
	<?php if ($carro) { ?>
	<?php
	foreach ($carro as $producto) {
	$unidades = $unidades + $producto['unidades'];
	?>
	<div class="col-xs-12">
		<div class="col-xs-4">
			<figure>
				<img src="<?php echo $producto["imagen"]; ?>" alt="" class="img-responsive">
			</figure>
		</div>
		<div class="col-xs-8">
			<h4><?php echo $producto["nombre"]; ?></h4>
			<span id="removeItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> - </span> <?php echo $producto["unidades"]; ?> <span id="addItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> + </span> x <?php echo number_format($producto["precio"], 2, ',', '.'); ?> Bs.
			<span class="eliminarProducto" data-unico="<?php echo $producto['unique_id']; ?>">Eliminar</span>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
</div>
<div class="totalCarrito">
	<div class="row">
		<div class="col-xs-12">
			<h4>Subtotal: <span><?php echo number_format($carrito->precio_total(), 2, ',', '.'); ?> Bs.</span></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php if (count($carro) > 5 || $unidades > 5) { ?>
			<a href="../../php/comprar.php" class="btn btn-carrito" data-estado="true">Procesar pedido</a>
			<?php } else { ?>
			<span class="btn btn-carrito" data-estado="false">Debes tener al menos 6 productos <br> para procesar el pedido</span>
			<?php } ?>
			<button id="vaciarCarrito" class="btn btn-carrito vaciar">Vaciar carrito</button>
		</div>
	</div>
</div>
<?php
} else {
$carro = $carrito->get_content();
$unidades = 0;
?>
<span class="icon-cancel-circle close"></span>
<div class="row">
	<?php if ($carro) { ?>
	<?php
	foreach ($carro as $producto) {
	$unidades = $unidades + $producto['unidades'];
	?>
	<div class="col-xs-12">
		<div class="col-xs-4">
			<figure>
				<img src="<?php echo $producto["imagen"]; ?>" alt="" class="img-responsive">
			</figure>
		</div>
		<div class="col-xs-8">
			<h4><?php echo $producto["nombre"]; ?></h4>
			<span id="removeItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> - </span> <?php echo $producto["unidades"]; ?> <span id="addItem" class="modificarItem" data-unico="<?php echo $producto['unique_id']; ?>"> + </span> x <?php echo number_format($producto["precio"], 2, ',', '.'); ?> Bs.
			<span class="eliminarProducto" data-unico="<?php echo $producto['unique_id']; ?>">Eliminar</span>
		</div>
	</div>
	<?php } ?>
	<?php } ?>
</div>
<div class="totalCarrito">
	<div class="row">
		<div class="col-xs-12">
			<h4>Subtotal: <span><?php echo number_format($carrito->precio_total(), 2, ',', '.'); ?> Bs.</span></h4>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<?php if (count($carro) > 5 || $unidades > 5) { ?>
			<a href="../../php/comprar.php" class="btn btn-carrito" data-estado="true">Procesar pedido</a>
			<?php } else { ?>
			<span class="btn btn-carrito" data-estado="false">Debes tener al menos 6 productos <br> para procesar el pedido</span>
			<?php } ?>
			<button id="vaciarCarrito" class="btn btn-carrito vaciar">Vaciar carrito</button>
		</div>
	</div>
</div>
<?php
}
?>