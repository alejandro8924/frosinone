<?php
require_once '../administrador/libs/Productos.php';
$productos = new Productos();
$detalles = json_decode($productos->detallesProducto($_GET['id']));
$imagenes = count($detalles->imagenes);
$col = 12 / $imagenes;

$relacionados = json_decode($productos->relacionados($_GET['id'], $detalles->nombre));
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Frosinone | <?php echo $detalles->nombre; ?></title>
		<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../../css/menu-dist.css">
		<link rel="stylesheet" type="text/css" href="../../css/styles.css">
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../../css/fonts.css">
	</head>
	<body>
		<div id="openCar">
			<span><span class="icon-basket"></span></span>
		</div>
		<div id="carrito">
		</div>
		<header class="main-header">
			<?php include '../inc/main-header.php'; ?>
		</header>
		<section id="articulo">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div id="images" styles="overflow: hidden;">
							<figure>
								<img src="../../administrador/images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[0]; ?>" data-zoom-image="../../administrador/images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[0]; ?>" alt="<?php echo $detalles->nombre; ?>" class="img-responsive">
							</figure>
						</div>
						<?php if ($imagenes > 1) { ?>
						<div id="gal1" class="row">
							<?php for ($i=0; $i < $imagenes; $i++) { ?>
							<div class="col-xs-<?php echo $col; ?>">
								<figure class="miniaturas">
									<a href="#" data-image="../../administrador/images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[$i]; ?>" data-zoom-image="../../administrador/images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[$i]; ?>">
										<img id="img_01" src="../../administrador/images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[$i]; ?>" alt="<?php echo $detalles->nombre; ?>" class="img-responsive">
									</a>
								</figure>
							</div>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
					<div class="col-sm-6">
						<h1><?php echo $detalles->nombre; ?></h1>
						<h3><?php echo number_format($detalles->precio, 2, ',', '.'); ?> Bs.</h3>
						<div class="descripcion">
							<span>Colores</span>
						</div>
						<!--
						<p><?php //echo $detalles->descripcion; ?></p>
						-->
						<div id="paleta">
							<ul>
								<li id="morado" data-color="morado"></li>
								<li id="amarillo" data-color="amarillo"></li>
								<li id="rojo" data-color="rojo"></li>
							</ul>
						</div>
						<br>
						<p>Cantidad: <input type="number" value="1" id="cantidad"></p>
						<button id="addCar" data-id="<?php echo $detalles->id; ?>" data-nombre="<?php echo $detalles->nombre; ?>" data-precio="<?php echo $detalles->precio; ?>" data-imagen="http://<?php echo $_SERVER['SERVER_NAME']; ?>/administrador/images/productos/<?php echo $detalles->url; ?>/<?php echo $detalles->imagenes[0]; ?>" class="btn btn-primary">Agregar al carrito</button>
						<div id="compartir">
							<a id="facebook" href="#">Compartir <span class="icon-facebook"></span></a>
							<a id="twitter" href="#">Compartir <span class="icon-twitter"></span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="relacionados">
			<?php if (count($relacionados) > 0 && count($relacionados) < 5) { ?>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h2>Productos relacionados</h2>
					</div>
				</div>
				<div class="row">
					<?php for ($i=0; $i < count($relacionados); $i++) { ?>
					<div class="col-sm-3">
						<div class="articulo">
							<a href="http://<?php echo $_SERVER['SERVER_NAME']; ?>/producto/<?php echo $relacionados[$i]->id; ?>/<?php echo $relacionados[$i]->url; ?>">
								<figure>
									<img src="../../administrador/images/productos/<?php echo $relacionados[$i]->url; ?>/<?php echo $relacionados[$i]->url; ?>-0.jpg" alt="<?php echo $relacionados[$i]->nombre; ?>" class="img-responsive">
								</figure>
								<span class="nombre"><?php echo $relacionados[$i]->nombre; ?></span>
								<br>
								<span class="precio"><?php echo $relacionados[$i]->precio; ?> Bs.</span>
							</a>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</section>
		<footer>
			<?php include '../inc/main-footer.php'; ?>
		</footer>
		<script type="text/javascript" src="../../js/jquery.min.js"></script>
		<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../../js/jquery.elevateZoom.min.js"></script>
		<script type="text/javascript" src="../../js/scripts.js"></script>
		<script type="text/javascript" src="../../js/carrito.js"></script>
	</body>
</html>