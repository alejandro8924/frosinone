<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Frosinone</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/menu-dist.css">
		<link rel="stylesheet" type="text/css" href="css/styles-dist.css">
		<!--<link rel="stylesheet" type="text/css" href="css/styles.css">-->
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
	</head>
	<body>
		<header class="main-header">
			<?php include 'inc/main-header.php'; ?>
		</header>
		<section id="login">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<?php if ($_GET['info'] == "success") { ?>
						<h2>¡Te registraste exitosamente!</h2>
						<p>Felicitaciones, ahora puedes comprar cualquier producto de nuestra tienda</p>
						<a href="index.php">Ir al inicio</a>
						<?php } else { ?>
						<h2>¡Ha ocurrido un error!</h2>
						<p>Lo siento, ha ocurrido un error, inténtalo de nuevo por favor</p>
						<a href="register.php">Volver a registrarme</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
	</body>
</html>