<div class="container">
	<div class="row">
		<div class="col-sm-3 col-md-3">
			<a href="http://localhost/frosinone/index.php">
				<img src="http://localhost/frosinone/img/Logo-blanco.png" alt="Frosinone" class="img-responsive">
			</a>
		</div>
		<div class="col-sm-3 col-md-3">
			<ul>
				<li><a href="http://localhost/frosinone/categoria/1/hombre">Hombre</a></li>
				<li><a href="http://localhost/frosinone/categoria/2/mujer">Mujer</a></li>
				<li><a href="http://localhost/frosinone/categoria/3/accesorios">Accesorios</a></li>
				<li><a href="http://localhost/frosinone/categoria/4/sport">Sport</a></li>
				<li><a href="http://localhost/frosinone/categoria/5/nylon">Nylon</a></li>
				<li><a href="http://localhost/frosinone/login.php">Entrar</a></li>
				<li><a href="http://localhost/frosinone/register.php">Registrate</a></li>
			</ul>
		</div>
		<div class="col-sm-3 col-md-3">
			<ul>
				<li><span class="icon-phone"></span> (+58) 274 1234567</li>
				<li><span class="icon-envelope"></span> info@frosinonefabrica.com</li>
			</ul>
			<p>Para compras al detal comunícate con nosotros</p>
		</div>
		<div class="col-sm-3 col-md-3">
			<h3>Siguenos en:</h3>
			<ul id="redes">
				<li><a href="https://www.facebook.com/frosinonefabrica/" target="_blank"><span class="icon-facebook"></span></a></li>
				<li><a href="#" target="_blank"><span class="icon-twitter"></span></a></li>
				<li><a href="#" target="_blank"><span class="icon-instagram"></span></a></li>
			</ul>
		</div>
	</div>
</div>