<?php
@session_start();
?>
<div class="container">
	<div class="row">
		<div class="col-xs-8 col-sm-3">
			<figure id="main-logo">
				<a href="http://localhost/frosinone/index.php">
					<img src="http://localhost/frosinone/img/Logo.png" alt="" class="img-responsive">
				</a>
			</figure>
		</div>
		<div class="col-sm-9">
			<ul id="main-menu" class="nav nav-pills hidden-xs">
				<li><a href="http://localhost/frosinone/categoria/1/hombre">Hombre</a></li>
				<li><a href="http://localhost/frosinone/categoria/2/mujer">Mujer</a></li>
				<li><a href="http://localhost/frosinone/categoria/3/accesorios">Accesorios</a></li>
				<li><a href="http://localhost/frosinone/categoria/4/sport">Sport</a></li>
				<li><a href="http://localhost/frosinone/categoria/5/nylon">Nylon</a></li>
				<li><a data-href="#quienesSomos">La fabrica</a></li>
				<?php if (!empty($_SESSION['id']) && !empty($_SESSION['nombre']) && !empty($_SESSION['correo'])) { ?>
				<li><a href="#"><?php echo $_SESSION['nombre']; ?></a></li>
				<li><a href="http://localhost/frosinone/php/logout.php">Salir</a></li>
				<?php } else { ?>
				<li><a href="http://localhost/frosinone/login.php">Entrar</a></li>
				<li><a href="http://localhost/frosinone/register.php">Registrate</a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>