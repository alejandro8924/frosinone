$('#carrito').load('../../php/carrito.php');

$.getJSON('php/carrito.php', function(json, textStatus) {
    $.each(json, function(index, val) {
        console.log(val);
        $('tbody').append('<tr><td>' + val.nombre + '</td><td style="text-align: center;">' + val.precio + '</td><td style="text-align: center;">' + val.unidades + '</td><td style="text-align: center;">Eliminar</td></tr>');
    });
});

var openCar = function() {
    $('#carrito').stop().animate({
            right: 0
        },
        200);
}

$('#openCar').click(openCar);

$('#carrito').on('click', '.close', function() {
    $('#carrito').stop().animate({
            right: -350
        },
        200);
});

$('#addCar').click(function() {
    console.log($('#cantidad').val());
    $.ajax({
        url: '../../php/carrito.php',
        type: 'POST',
        dataType: 'html',
        data: {
            id: $(this).data('id'),
            nombre: $(this).data('nombre'),
            precio: $(this).data('precio'),
            imagen: $(this).data('imagen'),
            cantidad: $('#cantidad').val()
        },
        beforeSend: function() {
            console.log('Cargando...');
        },
        success: function(data) {
            $('#carrito').html(data);
        },
        timeout: 4000,
        error: function(err) {
            console.log(err);
        }
    });

    openCar();
});


$('#carrito').on('click', '#vaciarCarrito', function() {
    $.ajax({
        url: '../../php/carrito.php',
        type: 'POST',
        dataType: 'html',
        data: {
            vaciar: true
        },
        beforeSend: function() {
            console.log('Cargando...');
        },
        success: function(data) {
            $('#carrito').html(data);
        },
        timeout: 4000,
        error: function(err) {
            console.log(err);
        }
    });
});

$('#carrito').on('click', '.eliminarProducto', function() {
    console.log($(this).data('unico'));
    $.ajax({
        url: '../../php/carrito.php',
        type: 'POST',
        dataType: 'html',
        data: {
            eliminarProducto: true,
            uniqueId: $(this).data('unico')
        },
        beforeSend: function() {
            console.log('Cargando...');
        },
        success: function(data) {
            $('#carrito').html(data);
        },
        timeout: 4000,
        error: function(err) {
            console.log(err);
        }
    });
});

$('#carrito').on('click', '.modificarItem', function() {
    console.log($(this).data('unico'));
    $.ajax({
        url: '../../php/carrito.php',
        type: 'POST',
        dataType: 'html',
        data: {
            modificarItem: true,
            accion: $(this).attr('id'),
            uniqueId: $(this).data('unico')
        },
        beforeSend: function() {
            console.log('Cargando...');
        },
        success: function(data) {
            $('#carrito').html(data);
        },
        timeout: 4000,
        error: function(err) {
            console.log(err);
        }
    });
});