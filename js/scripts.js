$('#articulo').on('click', '.miniaturas', function() {

    $('#images img').attr('src', $(this).children('img').attr('src'));

    //$('#images img').data('zoom-image', $(this).children('img').attr('src'));


});

/*
$('#paleta ul li').click(function() {

    if ($(this).data('color') == 'morado') {
        $('#images img').attr('src', '../../administrador/images/productos/cartera-magdalena/cartera-magdalena-0.jpg');
        $('#images img').data('zoom-image', '../../administrador/images/productos/cartera-magdalena/cartera-magdalena-0.jpg');
    } else if ($(this).data('color') == 'amarillo') {
        $('#images img').attr('src', '../../administrador/images/productos/cartera-magdalena/cartera-magdalena-0-amarillo.jpg');
        $('#images img').data('zoom-image', '../../administrador/images/productos/cartera-magdalena/cartera-magdalena-0-amarillo.jpg');
    } else if ($(this).data('color') == 'rojo') {
        $('#images img').attr('src', '../../administrador/images/productos/cartera-magdalena/cartera-magdalena-0-rojo.jpg');
        $('#images img').data('zoom-image', '../../administrador/images/productos/cartera-magdalena/cartera-magdalena-0-rojo.jpg');
    };
});
*/

$('#main-menu li a').click(function() {
    var position = $($(this).data('href')).position();
    $('body').stop().animate({
        scrollTop: (position.top - 89)
    }, {
        duration: 1000,
        specialEasing: {
            scrollTop: "easeOutCirc"
        }
    });
});

$('#images img').elevateZoom({
    gallery: 'gal1',
    cursor: 'pointer',
    galleryActiveClass: 'active',
    imageCrossfade: false,
    loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
});

//pass the images to Fancybox
$('#images img').bind("click", function(e) {
    var ez = $('#img_01').data('elevateZoom');
    $.fancybox(ez.getGalleryList());
    return false;
});