<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Document</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
		<style type="text/css">
		html, body {
			height: 100%;
		}
		body {
			font-family: 'Lato', sans-serif;
		}
		main {
			height: 100%;
			background-image: url('img/DSC_4373.jpg');
			background-size: cover;
			background-position: center center;
			background-repeat: no-repeat;
			position: relative;
		}
		h2 {
			color: #fff;
			font-size: 6.4rem;
			font-weight: 700;
			text-align: center;
			text-shadow: 0 2px 5px rgba(0, 0, 0, 0.50);
			margin: auto;
			position: absolute;
			left: 0;
			top: 45%;
			right: 0;
		}
		</style>
	</head>
	<body>
		<main>
			<h2>
				<img src="img/Logo-blanco.png" height="58" width="400" alt="">
				<br>
				Sitio en desarrollo
				<br>
			</h2>
		</main>
	</body>
</html>