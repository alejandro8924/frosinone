<?php
require_once '../administrador/libs/Productos.php';
$productos = new Productos();
$lista = json_decode($productos->categoria($_GET['id']));
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Frosinone | <?php echo strtoupper($_GET['url']); ?></title>
		<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../../css/menu-dist.css">
		<link rel="stylesheet" type="text/css" href="../../css/styles.css">
		<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="../../css/fonts.css">
	</head>
	<body>
		<div id="openCar">
			<span><span class="icon-basket"></span></span>
		</div>
		<div id="carrito">
		</div>
		<header class="main-header">
			<?php include '../inc/main-header.php'; ?>
		</header>
		<section id="bestSeller" style="margin-top: 120px;">
			<div class="container">
				<div class="row">
					<?php for ($i=0; $i < count($lista); $i++) { ?>
					<div class="col-sm-3">
						<div class="articulo">
							<a href="../../producto/<?php echo $lista[$i]->id; ?>/<?php echo $lista[$i]->url; ?>">
								<figure>
									<img src="../../administrador/images/productos/<?php echo $lista[$i]->url; ?>/<?php echo $lista[$i]->imagenes[0]; ?>" alt="<?php echo $lista[$i]->nombre; ?>" class="img-responsive">
								</figure>
								<span class="nombre"><?php echo $lista[$i]->nombre; ?></span>
								<br>
								<span class="precio"><?php echo $lista[$i]->precio; ?> Bs.</span>
							</a>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<footer>
			<?php include '../inc/main-footer.php'; ?>
		</footer>
		<script type="text/javascript" src="../../js/jquery.min.js"></script>
		<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../../js/jquery.elevateZoom.min.js"></script>
		<script type="text/javascript" src="../../js/scripts.js"></script>
		<script type="text/javascript" src="../../js/carrito.js"></script>
	</body>
</html>